package  libs

import (
	"encoding/json"
	"fmt"
)

type Group string

const (
	GroupContext = "@context"
	GroupTypeS   = "type"
	GroupName    = "name"
)

func DefineGroup(grr map[Group]string) map[Group]string {
	//	app.context = append(app.context, "@context")
	grr[GroupContext] = "https://www.w3.org/ns/activitystreams#Group"
	//	app.typeS = append(app.typeS, "type")
	grr[GroupTypeS] = "Group"
	//	app.name = append(app.name, "name")
	grr[GroupName] = "SnowCrashNetwork"
	return grr
}

func EncodeGroup() {
	//var app Application
	blob := make(map[Group]string)
	grr := DefineGroup(blob)

	jsonEncodedGroup, err := json.Marshal(grr)
	if err != nil {
		fmt.Println("Error marshaling json.")
	}
	fmt.Println(string(jsonEncodedGroup))
}
