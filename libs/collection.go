package libs

type CollectionS struct {
	Location   string
	Obj        ObjectS
	TotalItems int
	Current    []string
	First      string
	Last       string
	Items      []string
}

type OrderedCollectionS struct {
	Collection CollectionS
}
